﻿class Program1
{
    static void Main()
    {
TareaViewModel viewModel = new TareaViewModel();

        // Agregar algunos datos de ejemplo
        viewModel.AgregarTarea("Hacer la compra");
        viewModel.AgregarTarea("Estudiar para el examen");

        // Aquí contectamos el ViewModel a la interfaz de usuario (View)
        // Podemos usar un framework como WPF o Xamarin.Forms para esto.

        // Ejemplos de operaciones
        TareaModel tarea = viewModel.Tareas[0];
        viewModel.MarcarComoCompletada(tarea);

        // Visualizar las tareas (esto sería manejado por la interfaz de usuario en una aplicación real)
        foreach (var item in viewModel.Tareas)
        {
            Console.WriteLine($"{item.Titulo} - {(item.Completada ? "Completada" : "No Completada")}");
        }
        
    }
}