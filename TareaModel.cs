//Creamos la clase TareaModel 
//Representa la capa de datos y lógica de negocio
class TareaModel
{
    //propiedades básicas de la tarea
    //Título de la tarea
    public string Titulo { get; set; }
    
    //Saber si está completada o no
    public bool Completada { get; set; }
}