//Creamos la clase TareaViewModel
//Contiene la lógica de presentación y la lógica de interacción con la interfaz de usuario
//Se comunica con el Modelo 

using System;


using System.Collections.ObjectModel;

class TareaViewModel
{
    public ObservableCollection<TareaModel> Tareas { get; set; }
    public TareaViewModel()
    {
        Tareas = new ObservableCollection<TareaModel>();
    }

    //comportamiendo de agragar tarea
    public void AgregarTarea(string titulo)
    {
        TareaModel nuevaTarea = new TareaModel { Titulo = titulo, Completada = false };
        Tareas.Add(nuevaTarea);
    }

    //comportamiendo de marcar como completada una tarea
    public void MarcarComoCompletada(TareaModel tarea)
    {
        tarea.Completada = true;
    }

    //comportamiendo de marcar como no completada una tarea
    public void MarcarComoNoCompletada(TareaModel tarea)
    {
        tarea.Completada = false;
    }
    public void EliminarTarea(TareaModel tarea)
    {
        Tareas.Remove(tarea);
    }
}